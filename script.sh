#!/bin/bash

echo "Inciando a criacao das imagens"

sudo docker build -t jardeljga/aplicativo-projetodio:1.0 aplicativo/.
sudo docker build -t jardeljga/database-projetodio:1.0 database/.

echo "Iniciando Push das imagens para o dockerhub"

sudo docker push jardeljga/aplicativo-projetodio:1.0
sudo docker push jardeljga/database-projetodio:1.0

echo "iniciando criacao dos serviços do cluster"

kubectl apply -f ./services.yml

echo "realizando os deployments"

kubectl apply -f ./deployment.yml

